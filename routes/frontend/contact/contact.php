<?php

// Contact Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\Contact', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('contact', 'ContactController@index')->name('contact');
        Route::post('contact/store', 'ContactController@store')->name('contact.store');
    }
);
