<?php

// Admission Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\Admission', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('admission', 'AdmissionController@index')->name('admission');
    }
);
