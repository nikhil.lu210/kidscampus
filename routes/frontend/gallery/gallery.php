<?php

// Gallery Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\Gallery', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('gallery', 'GalleryController@index')->name('gallery');
    }
);
