<?php

// Curriculum Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\Curriculum', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('curriculum', 'CurriculumController@index')->name('curriculum');
    }
);
