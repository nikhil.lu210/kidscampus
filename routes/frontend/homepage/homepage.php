<?php

// Homepage Routes
Route::group([
    'prefix' => '/', //For Url
    'namespace' => 'Frontend\Homepage', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('', 'HomepageController@index')->name('homepage');
    }
);
