<?php

// Event Routes
Route::group([
    'prefix' => '', //For Url
    'namespace' => 'Frontend\Event', //For Controller
    'as' => '' //For Route Name
],
    function(){
        Route::get('event', 'EventController@index')->name('event');
    }
);
