<?php

// Auth::routes();
include_once 'auth/auth_route/auth_route.php';

// Frontend Routes (Guest)
Route::group(
    [
        'middleware' => ['guest' || 'auth']
    ],
    function () {

        // Homepage
        include_once 'frontend/homepage/homepage.php';

        // About
        include_once 'frontend/about/about.php';

        // Curriculum
        include_once 'frontend/curriculum/curriculum.php';

        // Admission
        include_once 'frontend/admission/admission.php';

        // Gallery
        include_once 'frontend/gallery/gallery.php';

        // Events
        include_once 'frontend/event/event.php';

        // Contact
        include_once 'frontend/contact/contact.php';

    }
);


// Backend Routes (Admin)
Route::group(
    [
        'middleware' => ['auth']
    ],
    function () {

        // Dashboard
        include_once 'backend/dashboard/dashboard.php';

        // contact
        include_once 'backend/contact/contact.php';
    }
);
