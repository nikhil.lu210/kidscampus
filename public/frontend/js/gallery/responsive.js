$(document).ready(function($) {
    function mediaSize() {
        /* Set the matchMedia */

        /*===============================================
        ==========< Device Maximum 575.98px >============
        =================================================*/
        if (window.matchMedia('(max-width: 575.98px)').matches) {
            // carousel part
            $('.welcome-side').removeClass('p-t-250').addClass('p-t-100');
            $('.row.font-open-sans.p-t-30.p-b-100').removeClass('p-t-30 p-b-100').addClass('p-t-20 p-b-20');
            $('.carousel-image-side').removeClass('p-t-200').addClass('p-t-20');

            // gallery part
            $('.col-md-3.col-sm-6.item').removeClass('col-md-3 col-sm-6').addClass('col-6');
        }


        /*=============================================================
        ==========< Device Size Between 576px to 767.98px >============
        =============================================================*/
        else if (window.matchMedia('(min-width: 576px)' && '(max-width: 768px)').matches) {
            // carousel part
            $('.welcome-side').removeClass('p-t-250').addClass('p-t-100');
            $('.carousel-image-side').removeClass('p-t-200').addClass('p-t-100');
        }


        /*=============================================================
        ==========< Device Size Between 768px to 991.98px >============
        ===============================================================*/
        else if (window.matchMedia('(min-width: 768.1px)' && '(max-width: 991.98px)').matches) {

        }


        /*==============================================================
        ==========< Device Size Between 992px to 1199.98px >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 992px)' && '(max-width: 1199.98px)').matches) {

        }


        /*================================================================
        ==========< Device Size Between 1200px to infinty :p >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 1200px)').matches) {

        }


        /*==========================================================
        ==========< Device Size if those are not match >============
        ==========================================================*/
        else {

        }
    };

    /* Call the function */
    mediaSize();
    /* Attach the function to the resize event listener */
    window.addEventListener('resize', mediaSize, false);

});
