$(document).ready(function($) {
    function mediaSize() {
        /* Set the matchMedia */

        /*===============================================
        ==========< Device Maximum 575.98px >============
        =================================================*/
        if (window.matchMedia('(max-width: 575.98px)').matches) {
            // carousel part
            $('.welcome-side').removeClass('p-t-250').addClass('p-t-100');
            $('.row.font-open-sans.p-t-30.p-b-100').removeClass('p-t-30 p-b-100').addClass('p-t-20 p-b-20');
            $('.carousel-image-side').removeClass('p-t-200').addClass('p-t-20');

            // about part
            $('.about-part').removeClass('p-t-100').addClass('p-t-10');
            $('.about-part .right-side').addClass('d-none');
            $('.btn-contact-us').removeClass('btn-block').addClass('float-right');

            // curriculum part
            $('.curriculum-details').removeClass('p-t-250').addClass('p-t-10');

            // admission part
            $('.admission-part').removeClass('p-t-200 p-b-100').addClass('p-t-50 p-b-10');
            $('.admission-part .col-md-4').addClass('m-b-20');

            // gallery part
            $('.col-md-3.col-sm-6.item').removeClass('col-md-3 col-sm-6').addClass('col-6');

            // event part
            $('.event-part').removeClass('p-t-100 p-b-100').addClass('p-t-30 p-b-20');

            // footer part
            $('.footer-part').removeClass('p-t-50 p-b-50').addClass('p-t-30 p-b-10');
        }


        /*=============================================================
        ==========< Device Size Between 576px to 767.98px >============
        =============================================================*/
        else if (window.matchMedia('(min-width: 576px)' && '(max-width: 768px)').matches) {
            // carousel part
            $('.welcome-side').removeClass('p-t-250').addClass('p-t-100');
            $('.carousel-image-side').removeClass('p-t-200').addClass('p-t-100');
            $('.row.font-open-sans.p-t-30.p-b-100').removeClass('p-t-30 p-b-100').addClass('p-t-20 p-b-20');

            // about part
            $('.about-part').removeClass('p-t-100').addClass('p-t-50');
            $('.btn-contact-us').removeClass('btn-block').addClass('float-right');

            // curriculum part
            $('.curriculum-details').removeClass('p-t-250').addClass('p-t-50');
            $('.curriculum-steps').parent().removeClass('col-md-6').addClass('col-md-5');
            $('.curriculum-details').parent().removeClass('col-md-5 offset-md-1').addClass('col-md-7');

            // admission part
            $('.admission-part').removeClass('p-t-200 p-b-100').addClass('p-t-100 p-b-10');

            // event part
            $('.event-part').removeClass('p-t-100 p-b-100').addClass('p-t-30 p-b-20');
            $('.event-details').parent().removeClass('col-md-5 offset-md-1').addClass('col-md-12');
            $('.event-part .img-part').addClass('d-none');

            // footer part
            $('.footer-part').removeClass('p-t-50 p-b-50').addClass('p-t-30 p-b-10');
        }


        /*=============================================================
        ==========< Device Size Between 768px to 991.98px >============
        ===============================================================*/
        else if (window.matchMedia('(min-width: 768.1px)' && '(max-width: 991.98px)').matches) {

        }


        /*==============================================================
        ==========< Device Size Between 992px to 1199.98px >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 992px)' && '(max-width: 1199.98px)').matches) {

        }


        /*================================================================
        ==========< Device Size Between 1200px to infinty :p >============
        ================================================================*/
        else if (window.matchMedia('(min-width: 1200px)').matches) {

        }


        /*==========================================================
        ==========< Device Size if those are not match >============
        ==========================================================*/
        else {

        }
    };

    /* Call the function */
    mediaSize();
    /* Attach the function to the resize event listener */
    window.addEventListener('resize', mediaSize, false);

});
