@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Dashboard')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')

<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                    <div class="flex-grow-1 d-flex align-items-center">
                        <div class="dot mr-3 bg-violet"></div>
                        <div class="text">
                            <h6 class="mb-0">Total Teachers</h6>
                            <span class="text-gray">12</span>
                        </div>
                    </div>
                    <div class="icon text-white bg-violet"><i class="fas fa-chalkboard-teacher"></i></div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-green"></div>
                    <div class="text">
                    <h6 class="mb-0">Total Students</h6><span class="text-gray">50</span>
                    </div>
                </div>
                <div class="icon text-white bg-green"><i class="fas fa-user-graduate"></i></div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-blue"></div>
                    <div class="text">
                    <h6 class="mb-0">Total Courses</h6><span class="text-gray">10</span>
                    </div>
                </div>
                <div class="icon text-white bg-blue"><i class="fas fa-atlas"></i></div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-red"></div>
                    <div class="text">
                    <h6 class="mb-0">Total Offers</h6><span class="text-gray">00</span>
                    </div>
                </div>
                <div class="icon text-white bg-red"><i class="fab fa-angellist"></i></div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
