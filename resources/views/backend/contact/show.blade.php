@extends('layouts.backend.app')
{{-- Page Title --}}
@section('page_title', 'Contacts II Details')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- =================< Contact Part Starts >================= --}}
<div class="container-fluid px-xl-5">
    <section class="py-5">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 mb-4">
                <div class="card shadow">
                    <div class="card-header">
                        <h6 class="text-uppercase mb-0 float-left">Contact Details</h6>

                        <a href="{{ route('admin.contact') }}" class="btn btn-outline-success btn-outline-custom btn-view btn-sm float-right">Back</a>
                    </div>
                    <div class="card-body">
                        <table class="table card-text table-borderless table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Father's Name</th>
                                    <td>{{ $contact->father }}</td>
                                </tr>

                                <tr>
                                    <th>Mother's Name</th>
                                    <td>{{ $contact->mother }}</td>
                                </tr>

                                <tr>
                                    <th>Kid's Name</th>
                                    <td>{{ $contact->kids }}</td>
                                </tr>

                                <tr>
                                    <th>Kid's Age</th>
                                    <td>{{ $contact->age }}</td>
                                </tr>

                                <tr>
                                    <th>Email Address</th>
                                    <td>{{ $contact->email }}</td>
                                </tr>

                                <tr>
                                    <th>Phone/Mobile Number</th>
                                    <td>{{ $contact->mobile }}</td>
                                </tr>

                                <tr>
                                    <th>Message</th>
                                    <td>{{ $contact->message }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
{{-- ==================< Contact Part Ends >================== --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script>

</script>
@endsection
