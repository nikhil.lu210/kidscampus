{{-- ============< Footer Part Start >=========== --}}
<footer class="footer-part p-t-50 p-b-20">
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-center">
                {{-- <h2 class="cy font-dosis">Kids <span class="cr">Campus</span></h2> --}}
                <img src="{{ asset('frontend/images/logo2.png') }}" class="img-responsive">
            </div>

            <div class="col-md-3">
                <h3 class="font-dosis">Address</h3>

                <p class="font-roboto p-t-10">80/A Bihango, Kazitula <br>Sylhet-3100, Bangladesh</p>
            </div>

            <div class="col-md-5">
                <h3 class="font-dosis">Contact</h3>

                <p class="font-roboto p-t-10">
                    <span class="phone">Phone: +880 1707 079651</span>
                    <br>
                    <span class="email">Email: kidscampussylhet@gmail.com</span>
                    <br>
                    <span class="email">Info: info@kidscampus.com.bd</span>
                </p>

                <ul class="social-icons d-inline-block p-t-10">
                    <li><a href="https://www.facebook.com/campus4kid" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://youtu.be/PCTRhbatm_Y" target="_blank"><i class="fab fa-youtube"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<section class="bottom-part p-t-30 p-b-20">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <ul class="menu-list font-dosis">
                    <li><a href="{{ Route('homepage') }}">Home</a></li>
                    <li><a href="{{ Route('about') }}">About</a></li>
                    <li><a href="{{ Route('curriculum') }}">Curriculum</a></li>
                    <li><a href="{{ Route('admission') }}">Admission</a></li>
                    <li><a href="{{ Route('gallery') }}">Gallery</a></li>
                    <li><a href="{{ Route('event') }}">Events</a></li>
                    <li><a href="{{ Route('contact') }}">Contact</a></li>
                </ul>
            </div>

            <div class="col-md-5">
                <p class="copyright font-dosis">Copuright 2019 || All rights Reserved || Developed by <a href="#" target="_blank">NIKHIL KURMI</a></p>
            </div>
        </div>
    </div>
</section>
{{-- =============< Footer Part End >============ --}}
