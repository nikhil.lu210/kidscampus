<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
{{-- CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

{{--  Page Title  --}}
<title> KIDS CAMPUS | @yield('page_title') </title>
<link rel="shortcut icon" href="{{ asset('frontend/images/logo.jpg') }}" type="image/x-icon">

{{--  Bootstrap CSS  --}}
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">

{{-- Fonts --}}
<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

{{--  Font-Awesome CSS  --}}
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome/all.min.css') }}">

@yield('css_links')

{{-- Custom CSS --}}
<link rel="stylesheet" href="{{ asset('frontend/css/margin_padding.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">


@yield('stylesheet')
