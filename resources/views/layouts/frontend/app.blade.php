<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.frontend.partials.header')
</head>

<body>
    @include('layouts.frontend.partials.navbar')

    <div class="main-body">
        @yield('content')
    </div>

    @include('layouts.frontend.partials.footer')

    @include('layouts.frontend.partials.script')
</body>

</html>
