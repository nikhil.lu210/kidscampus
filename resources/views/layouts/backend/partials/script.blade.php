<!-- JavaScript files-->
<script src="{{ asset('backend/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('backend/vendor/popper.js/umd/popper.min.js') }}"></script>
<script src="{{ asset('backend/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/vendor/jquery.cookie/jquery.cookie.js') }}"></script>
<script src="{{ asset('backend/vendor/chart.js/Chart.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
<script src="{{ asset('backend/js/front.js') }}"></script>

@yield('script_links')

{{-- Custom JS --}}
<script src="{{ asset('backend/js/script.js') }}"></script>
<script src="{{ asset('backend/js/responsive.js') }}"></script>

@yield('scripts')
