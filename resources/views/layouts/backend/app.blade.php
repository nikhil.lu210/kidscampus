<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.backend.partials.header')
</head>

<body>
    @include('layouts.backend.partials.topnav')

    <div class="d-flex align-items-stretch">
        @include('layouts.backend.partials.sidenav')

        @yield('content')
    </div>

    @include('layouts.backend.partials.footer')

    @include('layouts.backend.partials.script')
</body>

</html>
