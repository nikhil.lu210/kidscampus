@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'About')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/about/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/about/responsive.css') }}">
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ============< Carousel Part Start >=========== --}}
<section class="carousel-part" style="background-image: url('{{ asset('frontend/images/shapes/shape_01.png') }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="welcome-side font-dosis p-t-120">
                    <h1 class="cy">About</h1>
                    <h1 class="cr">Kids Campus</h1>
                </div>
            </div>

            <div class="col-md-6 offset-md-1">
                <div class="carousel-image-side p-t-100">
                    <img src="{{ asset('frontend/images/image_four.png') }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part End >============ --}}


{{-- =============< About Part Starts >============ --}}
<section class="about-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="about-details">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h1 class="cb font-dosis heading">About Us</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">Kids Campus International Preschool is the first flagship IPC franchise school in Sylhet adhering to high standards in International education. As an International Preschool Curriculum Franchise, we take pride in providing a high-quality and international preschool education in a safe and nurturing environment.
                                <br>
                                <br>
                            There are currently more than 200 IPC licensed school operating in 6 continents around the world. The curriculum is researched syllabus with a mixture of outcomes, objectives, play and inquiry-based learning.  All our teachers are carefully selected with qualified teaching backgrounds and are extensively trained by the IPC teacher training courses. Children are assessed individually using an assessment guide to track their progress to ensure that students are successfully meeting the learning outcomes in various areas such as language arts, social-emotional, numeracy, creative arts, sciences and motor skills.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="image-container"><img src="{{ asset('frontend/images/kids_about.png') }}" alt="" class="img-responsive"></div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< About Part Ends >============= --}}


{{-- =============< Our Strengths Part Starts >============ --}}
<section class="strength-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">

            <div class="col-md-5">
                <div class="image-container"><img src="{{ asset('frontend/images/gallery/image_5.jpg') }}" alt="" class="img-responsive"></div>
            </div>

            <div class="col-md-7">
                <div class="strength-details">
                    <div class="row">
                        <div class="col-md-12 p-t-20 p-b-10">
                            <h1 class="cb font-dosis heading">Our Strengths</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">All our teachers are selected through a rigorous recruitment process and are given an extensive teacher-training program because we believe all children deserve the best. <br> We make sure that each of our teaching staff has love for children, passion to teach, learn and grow together with each individual child. There will also be regular training arranged for our teachers to better their teaching skills, so as to upkeep our promise to you as providing the best education and learning experience for your child.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
{{-- ==============< Strengths Part Ends >============= --}}


{{-- =============< Parental Collaborations Part Starts >============ --}}
<section class="parental-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">

            <div class="col-md-7">
                <div class="parental-details">
                    <div class="row">
                        <div class="col-md-12 p-t-20 p-b-10">
                            <h1 class="cb font-dosis heading">Parental Collaborations</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">We believe that parents do make a difference in the child’s life and academic career when they get involve and collaborate with the teachers to provide support, creating a successful communication between classroom and home. When both parties have the same goal in mind when it comes to the child, both teacher-parents can become better partners in helping the child grow academically.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="image-container"><img src="{{ asset('frontend/images/gallery/parents.png') }}" alt="" class="img-responsive"></div>
            </div>

        </div>
    </div>
</section>
{{-- ==============< Parental Collaborations Part Ends >============= --}}


{{-- =============< Director’s Welcome Part Starts >============ --}}
<section class="director-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="director-details">
                    <div class="row">
                        <div class="col-md-12 p-t-20 p-b-10 text-center">
                            <h1 class="cb font-dosis heading">Director’s Welcome</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <p class="font-poppins">It is our utmost honour and pleasure to welcome you and your child to Kidsville International Preschool. The early years of childhood education is deemed important as we, at Kidsville, believe that children will benefit from improved social skills, accepting instructions better and being able to enhance their attention span in a school environment, preparing them for the real world. <br> Early childhood education plays a very important part in discovering your child’s potential. Through our program the child will be able to develop the desire to learn through our play and inquiry based approach. <br> We believe every child is unique and has their own imagination inspired by every little thing around them. Apart from academic learning, we believe that by working together with the parents will help cultivate much more interest within the child, making it a positive learning experience. <br> Therefore, let us end this note by thanking you for your interest in Kidsville and we hope that we will be able to establish this beautiful journey with you and your child in our aim to provide the best education experience ever. <br> Come on over and let’s bring the best out in your child !</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
{{-- ==============< Director’s Welcome Part Ends >============= --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/about/script.js') }}"></script>
<script src="{{ asset('frontend/js/about/responsive.js') }}"></script>
@endsection
