@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'Curriculum')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/curriculum/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/curriculum/responsive.css') }}">
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ============< Carousel Part Start >=========== --}}
<section class="carousel-part m-b-50" style="background-image: url('{{ asset('frontend/images/shapes/shape_01.png') }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="welcome-side font-dosis p-t-200">
                    <h1 class="cy">School</h1>
                    <h1 class="cr">Curriculum</h1>
                </div>
            </div>

            <div class="col-md-6 offset-md-1">
                <div class="carousel-image-side p-t-100">
                    <img src="{{ asset('frontend/images/curriculum.png') }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part End >============ --}}


{{-- =============< About Part Starts >============ --}}
<section class="about-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-details">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h1 class="cb font-dosis heading">The International Preschool Curriculum</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">The International Preschool Curriculum embodies thematic academic learning. The curriculum is based on proven and peer reviewed concepts that include play, inquiry and objective learning style. The IPC was devised and developed by a team of experienced early childhood educators and academics. IPC are designed to cultivate critical thinking, raise self-awareness, promotes an understanding of other cultures and encourage internationalism and multilingualism.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="image-container"><img src="{{ asset('frontend/images/ipc.png') }}" alt="" class="img-responsive"></div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< About Part Ends >============= --}}


{{-- =============< other Part Starts >============ --}}
<section class="other-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="other-details">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h1 class="cb font-dosis heading">Thematic Approach</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">Our programs are specially designed for each age group, where thematic approach is applied in our academic teaching. Kidsville will select an appropriate theme each term and integrate the theme with specially designed programs, so the children can acquire new skills and knowledge while enjoying learning at the same time. Thematic approach allows learning to be much more natural and fragmented, allowing literacy to grow and letting their ideas connect.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="other-details">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h1 class="cb font-dosis heading">Commitment</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">We, at Kidsville are committed to provide your child with a stimulating, challenging yet fun learning experience. <br>
                            Here, each child gets to cultivate their potential through independent thinking with our thematic approach, hence reinforcing their love of learning and enabling them to be confident both emotionally and socially.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< other Part Ends >============= --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/curriculum/script.js') }}"></script>
<script src="{{ asset('frontend/js/curriculum/responsive.js') }}"></script>
@endsection
