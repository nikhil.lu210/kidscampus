@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'Homepage')

{{--  External CSS Links --}}
@section('css_links')
<link rel="stylesheet" href="{{ asset('frontend/css/baguetteBox.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/grid-gallery.css') }}">
@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/homepage/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/homepage/responsive.css') }}">

<style>
.curriculum-part::before{
    content: url('../frontend/images/shapes/shape_03.png');
    position: absolute;
    left: -300px;
}
</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ============< Carousel Part Start >=========== --}}
<section class="carousel-part" style="background-image: url('{{ asset('frontend/images/shapes/shape_01.png') }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="welcome-side font-dosis p-t-250">
                    <h1 class="cy">Welcome to</h1>
                    <h1 class="cr">Kids Campus</h1>
                    <h5 class="cb">... Making Tomorrow</h5>
                </div>

                <div class="row font-open-sans p-t-30 p-b-100">
                    <div class="col-6">
                        <a href="#" class="btn btn-dark btn-lg btn-block btn-custom btn-know-us">Know Us</a>
                    </div>
                    <div class="col-6">
                        <a href="#" class="btn btn-dark btn-lg btn-block btn-custom btn-contact-us">Contact Us</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 offset-md-1">
                <div class="carousel-image-side p-t-200">
                    <img src="{{ asset('frontend/images/carousel.png') }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part End >============ --}}



{{-- ============< About Part Start >=========== --}}
<section class="about-part p-t-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-b-10">
                <h1 class="cb font-dosis">About Us</h1>
            </div>
            <div class="col-md-6 left-side">
                <p class="font-poppins">Kids Campus International Preschool is the first flagship IPC franchise school in Sylhet adhering to high standards in International education. As an International Preschool Curriculum Franchise, we take pride in providing a high-quality and international preschool education in a safe and nurturing environment.</p>
            </div>
            <div class="col-md-6 right-side">
                <p class="font-poppins">Kids Campus is the very first international preschool in Sylhet to follow the International Preschool Curriculum (IPC) which was devised and developed by a team of experienced early childhood educators and academics in United States.</p>
            </div>
            <div class="offset-md-6 col-md-6 p-t-20">
                <a href="#" class="btn btn-dark btn-lg btn-block btn-custom btn-contact-us">Know More</a>
            </div>
        </div>
    </div>
</section>
{{-- =============< About Part End >============ --}}



{{-- ============< Curriculum Part Start >=========== --}}
<section class="curriculum-part">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="curriculum-steps text-right p-t-50">
                    <img src="{{ asset('frontend/images/curriculum.png') }}" alt="" class="img-responsive">
                </div>
            </div>

            <div class="col-md-5 offset-md-1">
                <div class="curriculum-details p-t-100">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h1 class="cb font-dosis">Curriculum</h1>
                        </div>
                        <div class="col-md-12">
                            <p>There are currently more than 200 IPC licensed school operating in 6 continents around the world. The curriculum is researched syllabus with a mixture of outcomes, objectives, play and inquiry-based learning.  All our teachers are carefully selected with qualified teaching backgrounds and are extensively trained by the IPC teacher training courses.</p>
                        </div>
                        <div class="col-md-12">
                            <blockquote class="p-l-30 font-poppins">
                                <i>Kids Campus is the very first international preschool in Sylhet to follow the International Preschool Curriculum (IPC).</i>
                            </blockquote>
                        </div>
                        <div class="col-md-12 p-t-20">
                            <a href="#" class="btn btn-dark btn-lg btn-custom btn-know-us">Know Details</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Curriculum Part End >============ --}}



{{-- ============< Admission Part Start >=========== --}}
<section class="admission-part p-t-200 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card admission-procedure">
                    <div class="card-body">
                        <i class="fas fa-bezier-curve"></i>
                        <h4 class="font-dosis">Our Programs</h4>
                        <ul class="programs font-poppins">
                            <li><div class="float-left">Play</div><div class="float-right">2.5-4 years</div></li>
                            <li><div class="float-left">Pre-K</div><div class="float-right">4-5 years</div></li>
                            <li><div class="float-left">K</div><div class="float-right">5-6 years</div></li>
                            <li><div class="float-left">STD I-V</div><div class="float-right">Processing</div></li>
                        </ul>
                    </div>
                    <a href="#">
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">Learn More</div>
                                <div class="col-6 text-right"><i class="fas fa-arrow-right"></i></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card academic-fees">
                    <div class="card-body">
                        <i class="fab fa-angellist"></i>
                        <h4 class="font-dosis">Our Mission</h4>
                        <p class="font-poppins">Our Mission is to providing outstanding care to children while developing each child's personal, social, academic and creative needs.</p>
                    </div>
                    <a href="#">
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">Learn More</div>
                                <div class="col-6 text-right"><i class="fas fa-arrow-right"></i></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card admission-going-on">
                    <div class="card-body">
                        <i class="fas fa-building"></i>
                        <h4 class="font-dosis">Our Classroom</h4>
                        <ul class="programs font-poppins">
                            <li><div class="float-left">Mini Library</div><div class="float-right">Yes</div></li>
                            <li><div class="float-left">Student Locker</div><div class="float-right">Yes</div></li>
                            <li><div class="float-left">Soft Paly</div><div class="float-right">Yes</div></li>
                            <li><div class="float-left">Educational Resources</div><div class="float-right">Yes</div></li>
                        </ul>
                    </div>
                    <a href="#">
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">Learn More</div>
                                <div class="col-6 text-right"><i class="fas fa-arrow-right"></i></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Admission Part End >============ --}}



{{-- ============< Gallery Part Start >=========== --}}
<section class="gallery-part gallery-block grid-gallery">
    <div class="container">
        <div class="row p-b-50">
            <div class="col-md-12 text-center">
                <h1 class="cb font-dosis">Photo Gallery</h1>
                <p class="font-poppins">We capture all the memories of our childrens.</p>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_1.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_1.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_2.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_2.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_3.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_3.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_4.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_4.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_5.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_5.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_6.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_6.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_7.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_7.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_8.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_8.jpg') }}">
                </a>
            </div>

        </div>
    </div>
</section>
{{-- =============< Gallery Part End >============ --}}



{{-- ============< Events Part Start >=========== --}}
<section class="event-part p-t-100 p-b-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 offset-md-1">
                <div class="event-details p-b-30">
                    <h1 class="cb font-dosis">Our Events</h1>
                    <p class="font-poppins">Children are assessed individually using an assessment guide to track their progress to ensure that students are successfully meeting the learning outcomes in various areas such as language arts, social-emotional, numeracy, creative arts, sciences and motor skills.</p>
                </div>

                {{-- <table class="table event-one font-dosis">
                    <tr>
                        <td class="event-name">Annual Sports Day</td>
                        <td class="event-date">12 Feb 19</td>
                        <td class="event-details">
                            <a href="#">Details</a>
                        </td>
                    </tr>
                </table>

                <table class="table event-two font-dosis">
                    <tr>
                        <td class="event-name">Annual Sports Day</td>
                        <td class="event-date">12 Feb 19</td>
                        <td class="event-details">
                            <a href="#">Details</a>
                        </td>
                    </tr>
                </table>

                <table class="table event-three font-dosis">
                    <tr>
                        <td class="event-name">Annual Sports Day</td>
                        <td class="event-date">12 Feb 19</td>
                        <td class="event-details">
                            <a href="#">Details</a>
                        </td>
                    </tr>
                </table> --}}
            </div>

            <div class="col-md-5 offset-md-1 img-part" style="background-image: url('{{ asset('frontend/images/shapes/shape_04.png') }}')">
                <img src="{{ asset('frontend/images/event.png') }}" alt="Event Emaige Not Found" class="img-responsive">
            </div>
        </div>
    </div>
</section>
{{-- =============< Events Part End >============ --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')
<script src="{{ asset('frontend/js/baguetteBox.min.js') }}"></script>
@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/homepage/script.js') }}"></script>
<script src="{{ asset('frontend/js/homepage/responsive.js') }}"></script>
@endsection
