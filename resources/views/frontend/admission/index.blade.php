@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'Homepage')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/admission/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/admission/responsive.css') }}">
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ============< Carousel Part Start >=========== --}}
<section class="carousel-part m-b-50" style="background-image: url('{{ asset('frontend/images/shapes/shape_01.png') }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="welcome-side font-dosis p-t-150">
                    <h1 class="cy">Details About</h1>
                    <h1 class="cr">Admission</h1>
                </div>
            </div>

            <div class="col-md-6 offset-md-1">
                <div class="carousel-image-side p-t-150">
                    <img src="{{ asset('frontend/images/admission.png') }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part End >============ --}}


{{-- =============< admission Part Starts >============ --}}
<section class="admission-part-odd p-t-50 p-b-50">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="image-container"><img src="{{ asset('frontend/images/kids_one.png') }}" alt="" class="img-responsive"></div>
            </div>

            <div class="col-md-7">
                <div class="admission-details p-t-50">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h2 class="cb font-dosis heading">Play</h2>
                            <h5 class="cy font-dosis heading-moto">(2.5 years - 4 years)</h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">Your child, now, will be sponging up knowledge easily as they are at the age where curiosity gets the better of them, and where other than Kidsville, where we can make it a lifelong learning experience for your child. Kidsville will specifically introduce five senses to them and show them that there is a lot of knowledge to gain beyond, just by getting in touch with these senses of theirs, and by doing so; we are also developing their gross and motor skills. <br><br> The Nursery program is developed based on the understanding that the early formative years are the most critical in a child’s development. The curriculum prepared, therefore,will help them foster cognitive, speech and language development and also instilling self-help, patience, empathy and confidence in your child.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< admission Part Ends >============= --}}


{{-- =============< admission Part Starts >============ --}}
<section class="admission-part-even p-t-50 p-b-50">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="admission-details p-t-50">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h2 class="cb font-dosis heading">Pre-K</h2>
                            <h5 class="cy font-dosis heading-moto">(4 years - 5 years)</h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">As your child grows bigger, he/she tends to explore with their new found independence, and gain much more confidence by doing things by and for themselves. <br> This is where Kidsville comes in and help them expand their love for knowledge by allowing them to discover more through inquiry, building learning experiences by using touch, reasoning, play as a tool for the purposeful acquisition of knowledge and skills. <br> The Nursery 2 program focuses on primary areas of development such as language arts, literacy, mathematics, science, social and emotional development, gross and fine motor skills development, as well as character building. Your child will learn and develop some of these life skills through our Nursery 2 program.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="image-container"><img src="{{ asset('frontend/images/kids_two.png') }}" alt="" class="img-responsive"></div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< admission Part Ends >============= --}}


{{-- =============< admission Part Starts >============ --}}
<section class="admission-part-odd p-t-50 p-b-50">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="image-container"><img src="{{ asset('frontend/images/kids_three.png') }}" alt="" class="img-responsive"></div>
            </div>

            <div class="col-md-7">
                <div class="admission-details p-t-50">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h2 class="cb font-dosis heading">K</h2>
                            <h5 class="cy font-dosis heading-moto">(5 years - 6 years)</h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">The knowledge and skills that these children gain in preschool is very important for their future as whatever learning and social skills they acquire today are the very basic foundation that will shape your child’s future in later years. <br><br> Kidsville offers approved Kindergarten program that complies to our National Preschool Curriculum Standard (KSSR), yet still keeping to learning through our International Preschool Curriculum thematic approach. It does not only enhance confidence but enable children to become problem-solves at an early age. <br> Our Kindergarten program consists of hands-on learning, structured and routine activities, where the children will develop a variety of skills and knowledge in subjects, such as mathematics, science, English, Mandarin, Malay, and social skills. This is also when we prepare our pre-schoolers who will soon be age-ready to attend national/private or international primary schools.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< admission Part Ends >============= --}}


{{-- =============< admission Part Starts >============ --}}
<section class="admission-part-even p-t-50 p-b-50">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="admission-details p-t-50">
                    <div class="row">
                        <div class="col-md-12 p-b-10">
                            <h2 class="cb font-dosis heading">STD I-V</h2>
                            <h5 class="cy font-dosis heading-moto">(Processing)</h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="font-poppins">Your child, now, will be sponging up knowledge easily as they are at the age where curiosity gets the better of them, and where other than Kidsville, where we can make it a lifelong learning experience for your child. Kidsville will specifically introduce five senses to them and show them that there is a lot of knowledge to gain beyond, just by getting in touch with these senses of theirs, and by doing so; we are also developing their gross and motor skills. <br><br> The Nursery program is developed based on the understanding that the early formative years are the most critical in a child’s development. The curriculum prepared, therefore,will help them foster cognitive, speech and language development and also instilling self-help, patience, empathy and confidence in your child.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="image-container"><img src="{{ asset('frontend/images/kids_four.png') }}" alt="" class="img-responsive"></div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< admission Part Ends >============= --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/admission/script.js') }}"></script>
<script src="{{ asset('frontend/js/admission/responsive.js') }}"></script>
<script>

</script>
@endsection
