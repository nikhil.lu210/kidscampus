@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'Contact')

{{--  External CSS Links --}}
@section('css_links')

@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/contact/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/contact/responsive.css') }}">
<style>

</style>
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ============< Carousel Part Start >=========== --}}
<section class="carousel-part" style="background-image: url('{{ asset('frontend/images/shapes/shape_01.png') }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="welcome-side font-dosis p-t-200">
                    <h1 class="cy">Contact</h1>
                    <h1 class="cr">With Us</h1>
                </div>
            </div>

            <div class="col-md-6 offset-md-1">
                <div class="carousel-image-side p-t-100">
                    <img src="{{ asset('frontend/images/contact.png') }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part End >============ --}}


{{-- =============< contact Part Starts >============ --}}
<section class="contact-part p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-details">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 class="cb font-dosis heading">Contact Us</h1>
                        </div>
                    </div>

                    <div class="row p-t-20">
                        <div class="col-md-8 offset-md-2 font-dosis">
                            <form action="{{ Route('contact.store') }}" method="post" class="contact-form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="father">Father Name <span class="cr">*</span></label>
                                            <input type="text" class="form-control form-control-lg font-poppins" id="father" name="father" aria-describedby="fatherNameHelp" placeholder="Mr. Jhon Doe" required>
                                            <small id="fatherNameHelp" class="form-text text-muted">Your Kids Father's Name</small>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mother">Mother Name <span class="cr">*</span></label>
                                            <input type="text" class="form-control form-control-lg font-poppins" id="mother" name="mother" aria-describedby="motherNameHelp" placeholder="Mrs. Jhon Doe" required>
                                            <small id="motherNameHelp" class="form-text text-muted">Your Kids Mother's Name</small>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="kids">Kids Name <span class="cr">*</span></label>
                                            <input type="text" class="form-control form-control-lg font-poppins" id="kids" name="kids" aria-describedby="kidsNameHelp" placeholder="Kiddy Lily" required>
                                            <small id="kidsNameHelp" class="form-text text-muted">Your Kids Name</small>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="age">Kids Age <span class="cr">*</span></label>
                                            <input type="text" class="form-control form-control-lg font-poppins" id="kids" name="age" aria-describedby="ageHelp" placeholder="2 Years" required>
                                            <small id="ageHelp" class="form-text text-muted">Your Kids Age</small>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control form-control-lg font-poppins" id="kids" name="email" aria-describedby="emailHelp" placeholder="parent@mail.com">
                                            <small id="emailHelp" class="form-text text-muted">Your Valid Email Address</small>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mobile">Phone / Mobile No. <span class="cr">*</span></label>
                                            <input type="text" class="form-control form-control-lg font-poppins" id="kids" name="mobile" aria-describedby="mobileHelp" placeholder="+880 1234 567890" required>
                                            <small id="mobileHelp" class="form-text text-muted">Your Phone or Mobile Number</small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message">Message <span class="cr">*</span></label>
                                            <textarea class="form-control form-control-lg font-poppins" id="message" name="message" rows="3" placeholder="Your Message"></textarea>
                                            <small id="messageHelp" class="form-text text-muted">Your Message</small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-dark btn-lg btn-block btn-custom btn-know-us float-right">Send Message</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ==============< contact Part Ends >============= --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')

@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/contact/script.js') }}"></script>
<script src="{{ asset('frontend/js/contact/responsive.js') }}"></script>
@endsection
