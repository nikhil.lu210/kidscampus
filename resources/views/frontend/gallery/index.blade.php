@extends('layouts.frontend.app')
{{-- Page Title --}}
@section('page_title', 'Homepage')

{{--  External CSS Links --}}
@section('css_links')
<link rel="stylesheet" href="{{ asset('frontend/css/baguetteBox.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/grid-gallery.css') }}">
@endsection
{{--  External CSS  --}}
@section('stylesheet')
<link rel="stylesheet" href="{{ asset('frontend/css/gallery/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/gallery/responsive.css') }}">
@endsection


{{-- Body Parts From Here --}}
@section('content')
{{-- ============< Carousel Part Start >=========== --}}
<section class="carousel-part" style="background-image: url('{{ asset('frontend/images/shapes/shape_01.png') }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="welcome-side font-dosis p-t-200">
                    <h1 class="cy">Kids Campus</h1>
                    <h1 class="cr">Gallery</h1>
                </div>
            </div>

            <div class="col-md-6 offset-md-1">
                <div class="carousel-image-side p-t-100">
                    <img src="{{ asset('frontend/images/carousel.png') }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part End >============ --}}



{{-- ============< Gallery Part Start >=========== --}}
<section class="gallery-part video-gallery p-t-100">
    <div class="container">
        <div class="row p-b-50">
            <div class="col-md-12 text-center">
                <h1 class="cb font-dosis">Video Gallery</h1>
                <p class="font-poppins">We capture all the memories of our childrens.</p>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6 col-sm-12 video-item">
                <iframe src="https://www.youtube.com/embed/PCTRhbatm_Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="true"></iframe>
            </div>

            <div class="col-md-6 col-sm-12 video-item">
                <iframe src="https://www.youtube.com/embed/PCTRhbatm_Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="true"></iframe>
            </div>

        </div>
    </div>
</section>
{{-- =============< Gallery Part End >============ --}}



{{-- ============< Gallery Part Start >=========== --}}
<section class="gallery-part gallery-block grid-gallery">
    <div class="container">
        <div class="row p-b-50">
            <div class="col-md-12 text-center">
                <h1 class="cb font-dosis">Photo Gallery</h1>
                <p class="font-poppins">We capture all the memories of our childrens.</p>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_1.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_1.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_2.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_2.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_3.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_3.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_4.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_4.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_5.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_5.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_6.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_6.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_7.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_7.jpg') }}">
                </a>
            </div>

            <div class="col-md-3 col-sm-6 item">
                <a class="lightbox" href="{{ asset('frontend/images/gallery/image_8.jpg') }}">
                    <img class="img-fluid image scale-on-hover" src="{{ asset('frontend/images/gallery/image_8.jpg') }}">
                </a>
            </div>

        </div>
    </div>
</section>
{{-- =============< Gallery Part End >============ --}}
@endsection


{{--  External Javascript Links --}}
@section('script_links')
<script src="{{ asset('frontend/js/baguetteBox.min.js') }}"></script>
@endsection

{{--  External Javascript  --}}
@section('scripts')
<script src="{{ asset('frontend/js/gallery/script.js') }}"></script>
<script src="{{ asset('frontend/js/gallery/responsive.js') }}"></script>
@endsection
