<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'KC ADMIN',
            'email' => 'kids.campus@admin.com',
            'password' => bcrypt('12345678')
        ]);
    }
}
