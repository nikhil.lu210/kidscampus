<?php

namespace App\Model\Backend;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'father',
        'mother',
        'kids',
        'age',
        'email',
        'phone',
        'message',
    ];
}
